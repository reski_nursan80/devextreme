import React, { createContext, useReducer, useState } from 'react';
import Api from '../Services/ApiFasilitas';
export const FasilitasContext = createContext();

function FasilitasProvider({ children }) {
    const [fasilitasAll,setFasilitasAll] = useState([])

    function getdatafasilitasAll() {
          Api.getFasilitasAll().then(res => {
            setFasilitasAll(res.data)
            // console.log('data fasilitas', res.data)
          })
        
    }

    return(
        <FasilitasContext.Provider
        value={{
          fasilitasAll,
          getdatafasilitasAll,    
        }}
        >
           {children}
        </FasilitasContext.Provider>
      )
}
export default FasilitasProvider;