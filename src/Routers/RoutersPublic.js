import React from "react";
import { Route, withRouter, Switch } from "react-router-dom";
import { Grid } from "@material-ui/core";

//Pages
import Example2 from '../Pages/Example2/Example2'
import Home from '../Pages/Home/Home'

function Routers() {
    return(
        <>
        <Grid container>
            <Grid md={12}>
                <Switch>
                <Route exact path="/" component={Home} />
                </Switch>
            </Grid>
        </Grid>
        </>
    )
}
export default withRouter(Routers);