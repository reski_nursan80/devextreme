import axios from "axios";
import StaticVar from "../Config/StaticVar";

// ===> api create
const api = axios.create({
  baseURL: StaticVar.URL_API_FASILITAS,
  // timeout: 10000,
  headers: {},
});

// ===> api interceptors
api.interceptors.request.use(
  function (config) {
    // set headers after authentication
    config.headers["x-access-token"] = localStorage.getItem("token");
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

const getFasilitasAll = (data) => api.get("/profile/fasilitas/all", data);
const postFasilitas = (data) => api.post("/profile/register", data);
const putFasilitas = (id, data) => api.put("/profile/" + id, data);
const deleteFasilitas = (id) => api.delete("/profile/" + id);

export const apis = {
    getFasilitasAll,
    postFasilitas,
    putFasilitas,
    deleteFasilitas
  };
  
  export default apis;