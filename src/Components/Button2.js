import React from 'react';
import { Box, Button, Typography } from '@material-ui/core';

export default function Button2(props) {
  return (
    <Button
      size={props.size}
      startIcon={props.icons}
      disabled={props.disabled}
      type={props.type}
      style={{
        backgroundColor: props.colorUtama,
        color: props.colorText,
        marginTop: props.top,
        width: props.width,
      }}
      onClick={props.onClick}
    >
      {props.text}
    </Button>
  );
}
