import React from 'react';
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  Typography,
  IconButton,
  Tabs,
  AppBar
} from '@material-ui/core';
import Toolbar from '@material-ui/core/Toolbar';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
// Icon

export default function Dialogs(props) {
  return (
    <>
      <Dialog
        open={props.open}
        onClose={props.onClose}
        fullWidth={props.fullWidth}
        fullScreen={props.fullScreen}
        maxWidth={props.maxWidth}
      >
        <div style={{ backgroundColor: 'white' }}>
          <Toolbar>
            <IconButton
              edge='start'
              color='inherit'
              onClick={props.onClickClose}
              aria-label='close'
            >
              <ArrowBackIcon style={{ color: 'black' }} />
            </IconButton>
            <Typography
              style={{ fontWeight: 'bold', color: 'gray', fontSize: 18 }}
            >
              {' '}
              {props.titles}
            </Typography>
          </Toolbar>
        </div>
        
        <Divider />
        <DialogContent style={{ backgroundColor: props.colorContent }}>
          {props.content}
        </DialogContent>
        <DialogActions>{props.action}</DialogActions>
      </Dialog>
    </>
  );
}
