import React, {useState} from 'react'

import {
    ButtonGroup,
    Button,
    Typography
  } from "@material-ui/core";

import Example1 from '../Example1/Example1'
import Example2 from '../Example2/Example2'
import Example3 from '../Example3/Example3';

export default function Home() {
    const [panel, setpanel] = useState('Versi 1')

    return (
        <div style={{padding: 20}}>
            <Typography style={{paddingBottom: 10, fontWeight: 'bold'}}>DEVEXTREME CRUD</Typography>
            <ButtonGroup style={{ width: '100%' }}>
                <Button style={{ width: '30%' }} variant="contained" color={panel === 'Versi 1' ? 'secondary' : 'default'} onClick={() => setpanel("Versi 1")}>Versi 1</Button>
                <Button style={{ width: '40%' }} variant="contained" color={panel === 'Versi 2' ? 'secondary' : 'default'} onClick={() => setpanel("Versi 2")}>Versi 2</Button>
                <Button style={{ width: '30%' }} variant="contained" color={panel === 'Versi 3' ? 'secondary' : 'default'} onClick={() => setpanel("Versi 3")}>Versi 3</Button>
            </ButtonGroup>
           
            {
                panel === 'Versi 1' ? (
                    <div>
                        <Example1/>
                    </div>
                ) : panel === 'Versi 2' ? (
                    <div>
                        <Example2/>
                    </div>
                ) : panel === 'Versi 3' ? (
                    <Example3/>
                ) : null
            }
        </div>
    )
}
