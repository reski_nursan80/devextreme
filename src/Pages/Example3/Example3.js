import React, { useState, useContext,useEffect } from 'react'
import _, { values } from "lodash";

import {
    DataGrid, Column, Editing, Paging, FilterRow, Export,  Grouping, GroupPanel,
    SearchPanel, ColumnChooser, LoadPanel,
  } from 'devextreme-react/data-grid';
import { Popup } from "devextreme-react/popup";
import {
    Box,
    TextField,
    Typography,
  } from "@material-ui/core";
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';


// ====== API =========
import {FasilitasContext} from "../../Context/FasilitasContext"
import Api from '../../Services/ApiFasilitas';
import CustomStore from 'devextreme/data/custom_store';

export default function Example3() {
    const [onVisible, setOnVisible] = React.useState(false);
    const [formName, setFormName] = useState('')
    const [formKlaster, setFormKlaster] = useState('')
    const [formKategori, setFormKategori] = useState('')

    const handleAddNew = () => {
        setOnVisible(true);
      };

    function onToolbarPreparing(e) {
    let toolsItems = e.toolbarOptions.items;

    for (let i = 0; i < toolsItems.length; i++) {
        let item = toolsItems[i];
        if (item.name === "addRowButton") {
        item.options.onClick = handleAddNew;
        break;
        }
    }
      }
    
    const { 
        fasilitasAll,
        getdatafasilitasAll,
    } = useContext(FasilitasContext)

    const dataFasilitas = fasilitasAll.map((item,index) => {
        let capasity = _.sumBy(item.room, "capacity")
        let booking = item.room.filter(x => x.booking.length > 0)
        return (
            {
                _id: item._id,
                name: item.name,
                cluster: item.cluster,
                type: item.type,
                // jumlah: item.room.length,
                // kapasitas: capasity,
                // terbooking: booking.length
            }
        )
      } );

    const dataFasilitas1 = new CustomStore({
        key: '_id',
        load: () => dataFasilitas,
        // insert: (values) =>  Api.postFasilitas(values).then(res => {
        //     getdatafasilitasAll();
        // }),
        update: (key, postData) =>  Api.putFasilitas(key, postData ).then(res => {
            getdatafasilitasAll();
        }),
        remove: (key) =>  Api.deleteFasilitas(key).then((res) => {
            getdatafasilitasAll();
          })

    })

    useEffect(() => {
    getdatafasilitasAll();
    }, []);

    const handleAddData = () => {
        var postData = {
            name: formName,
            cluster: formKlaster,
            type: formKategori
        }
        Api.postFasilitas(postData).then(res => {
            getdatafasilitasAll();
            alert('Berhasil Tambah Fasilitas Baru!');
            setOnVisible(false)
        });
    }

    return (
        <div style={{padding: 20}}>
            DEVEXTREME VERSI 3
            <div>
                <DataGrid
                id="grid"
                showBorders={true}
                dataSource={dataFasilitas1}
                repaintChangesOnly={true}
                columnAutoWidth={true}
                allowColumnReordering={true}
                onToolbarPreparing={onToolbarPreparing}
                >
                <Editing
                    mode="popup"
                    allowAdding={true}
                    allowDeleting={true}
                    allowUpdating={true}>
                </Editing>
                <FilterRow visible={true} />
                <Grouping autoExpandAll={true} />
                <GroupPanel
                    visible={true}
                    emptyPanelText="Pilih Kolom untuk grouping"
                />
                <Export enabled={true} fileName="Laporan Booking" />
                <LoadPanel enabled={true} height="50" />
                <SearchPanel visible={true} highlightCaseSensitive={true} />
                <Paging pageSize="50" />
                <ColumnChooser />
                <Column dataField="name" caption="Nama Fasilitas" />
                <Column dataField="cluster" caption="Klaster" />
                <Column dataField="type" caption="Kategory" />
                </DataGrid>
                <Popup title="Tambah Data" showTitle={true} width={700}
                     showCloseButton={false} visible={onVisible}>
                    <Box 
                        component="form" 
                        setOnVisible={() => setOnVisible(false)}
                    >
                        <Typography style={{paddingBottom: 5}}>Nama Lengkap</Typography>
                        <TextField 
                            value={formName}
                            onChange={(e) => {setFormName(e.target.value)}}
                            style={{width: '100%', paddingBottom: 10}} 
                            size="small" 
                            placeholder="Masukkan Nama Lengkap Anda" 
                            variant="outlined" />
                        <Typography style={{paddingBottom: 5}}>Klaster</Typography>
                        <TextField
                            size='small'
                            select
                            InputLabelProps={{shrink: true,}}
                            SelectProps={{native: true,}}
                            variant='outlined'
                            style={{width: '100%', paddingBottom: 10}}
                            value={formKlaster}
                            onChange={(e) => {setFormKlaster(e.target.value)}}
                        >
                            <option value=''>Pilih Klaster Anda</option>
                            <option value='Kota Jayapura'>Kota Jayapura</option>
                            <option value='Kabupaten Jayapura'>Kabupaten Jayapura</option>
                            <option value='Kabupaten Mimika'>Kabupaten Mimika</option>
                            <option value='Kabupaten Merauke'>Kabupaten Merauke</option>
                        </TextField>
                        <Typography style={{paddingBottom: 5}}>Kategori</Typography>
                        <TextField
                            size='small'
                            select
                            InputLabelProps={{shrink: true,}}
                            SelectProps={{native: true,}}
                            variant='outlined'
                            style={{width: '100%', paddingBottom: 10}}
                            value={formKategori}
                            onChange={(e) => {setFormKategori(e.target.value)}}
                        >
                            <option value=''>Pilih Kategori</option>
                            <option value='Hotel'>Hotel</option>
                            <option value='Non Hotel'>Non Hotel</option>
                        </TextField>
                        <Stack direction='row' spacing={2} justifyContent='flex-end' style={{marginTop: "20%"}}>
                            <Button 
                                variant="contained" 
                                style={{backgroundColor: 'blue'}}
                                onClick={() => {handleAddData()}}
                            >
                                Save
                            </Button>
                            <Button variant="contained" style={{backgroundColor: 'gray'}} onClick={() => setOnVisible(false)}>Cancel</Button>
                        </Stack>
                    </Box>
                </Popup>
            </div>
        </div>
    )
}
