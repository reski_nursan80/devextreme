import React, { useState, useContext,useEffect } from 'react'
import _, { values } from "lodash";

import {
    DataGrid, Column, Editing, Scrolling, Lookup, Summary, TotalItem,Paging, FilterRow, HeaderFilter, Export,  Grouping, GroupPanel,
    SearchPanel, ColumnChooser, LoadPanel, Button,
  } from 'devextreme-react/data-grid';

// ====== API =========
import {FasilitasContext} from "../../Context/FasilitasContext"
import Api from '../../Services/ApiFasilitas';
import CustomStore from 'devextreme/data/custom_store';

export default function Example2() {
    
    const { 
        fasilitasAll,
        getdatafasilitasAll,
    } = useContext(FasilitasContext)

    const dataFasilitas = fasilitasAll.map((item,index) => {
        let capasity = _.sumBy(item.room, "capacity")
        let booking = item.room.filter(x => x.booking.length > 0)
        return (
            {
                _id: item._id,
                name: item.name,
                cluster: item.cluster,
                type: item.type,
                jumlah: item.room.length,
                kapasitas: capasity,
                terbooking: booking.length
            }
        )
      } );

    const dataFasilitas1 = new CustomStore({
        key: '_id',
        load: () => dataFasilitas,
        insert: (values) =>  Api.postFasilitas(values).then(res => {
            getdatafasilitasAll();
        }),
        update: (key, postData) =>  Api.putFasilitas(key, postData ).then(res => {
            getdatafasilitasAll();
        }),
        remove: (key) =>  Api.deleteFasilitas(key).then((res) => {
            getdatafasilitasAll();
          })

    })


    useEffect(() => {
    getdatafasilitasAll();
    }, []);

    return (
        <div style={{padding: 20}}>
            DEVEXTREME VERSI 2
            <div>
                <DataGrid
                id="grid"
                showBorders={true}
                dataSource={dataFasilitas1}
                repaintChangesOnly={true}
                columnAutoWidth={true}
                allowColumnReordering={true}
                >
                <Editing
                    // refreshMode={refreshMode}
                    mode="cell"
                    allowAdding={true}
                    allowDeleting={true}
                    allowUpdating={true}
                />
                <FilterRow visible={true} />
                <Grouping autoExpandAll={true} />
                <GroupPanel
                    visible={true}
                    emptyPanelText="Pilih Kolom untuk grouping"
                />
                <Export enabled={true} fileName="Laporan Booking" />
                <LoadPanel enabled={true} height="50" />
                <SearchPanel visible={true} highlightCaseSensitive={true} />
                <Paging pageSize="50" />
                <ColumnChooser />
                <Column dataField="name" caption="Nama Fasilitas" />
                <Column dataField="cluster" caption="Klaster" />
                <Column dataField="type" caption="Kategory" />
                </DataGrid>
            </div>
        </div>
    )
}
